---
layout: post
title: "#MercrediFiction. La punition de la machine par vanité humaine et le supplice d'Arendt"
date: 2019-01-29 15:05:14 +0100
location: Nowhere
---

C'était le jour des vents de Gabriel, pluies froides et infini de gris. La journée avait pourtant bien débuté, café comme  à l'accoutumé, mais avec cette étrange sensation. Le podcasteur protestant passa me voir, café discussion, interview, tout allait routinalement bien à l'abri des affres des vents de l'hiver. Nous parlions lettres et technologies, églises et prophètes, totalitarisme et responsabilité politique.

> « _Ces expressions toutes faites, utilisées mécaniquement, empêchent l'imagination, elles entrainent une incapacité à être affecté par ce que l'on fait et, la personne se drapant dans un aspect banal, entretiennent l'absence de pensée_ » Absence de pensée et responsabilité chez [Hannah Arendt − À propos d’Eichmann](http://www.raison-publique.fr/article606.html)

Un jour ordinaire... avec des affres dans les vents, avec du totalitarisme ? J'aurais sentir s'approcher le drame, le parfum d'un diable caché, occupant d'un humain, jusqu'à l'ouverture de sa fenêtre de tir.

C'est un Louarn[^1] qui débarqua d'un volumen du web, petite chose enroulée, aussi appelée techonologie ou transformation digitale par des adeptes du poop numérique. De la fourberie de ce je venais de lire qu'il s'agit maintenant de parler de « deepfake »[^3], plus puissant que et concepteur de « fakenews ». Avec « deeptech » et « deepweb » on peut démarrer un jeu des 7 deepfamilles ? 

Manipuler une information dans un système par biais permi d'un usage avancé technologique était déjà « deep » en 1834[^4] ? Ou l'on abandonne de facto notre pouvoir de penser et donc notre conscience des actes face à pouvoir occulte qui masque notre responsabilité politique ?


**Rahhhhh**, piqué par la colère d'être toujours et encore surpris, je m'énervais contre le pauvre dont l'être et le corps devaient être sous l'occupation d'un diable qui le rendait si vulnérable aux algorithmes et cupide de l'infobésité. Et ma machine à moi, avec ces configurations dont je prends si soin, ne pouvait pas me protéger de cet effet Venturi[^5] sur les capacités de langage et responsabilités liées à ce langage ? C'est la faute de la machine ! C'est la faute de la technologie ! Nous les humains n'exécutons rien machinalement, nous sommes dans les petites choses, une quotidienneté, mais avec des capacités de critiques et de raison et celles et ceux qui logent dans les professions de folies et de novlang des églises IA ne peuvent nous convaincre. Nous sommes sain⋅e⋅s et nous sommes libres. D'ailleurs, Hannah Arendt montra que l'usage des clichés de langage diminuent la conscience des actes. Ces expressions toutes faites, utilisées mécaniquement, empêchent l'imagination, elles entrainent une incapacité à être affecté par ce que l'on fait et, la personne se drapant dans un aspect banal, entretiennent l'absence de pensée.

Alors même qu'aujourd'hui l'imprégnation idéologique des exécuteurs de commande des prophètes et de tyrans est considérée comme plus importante que ce qu'en décrivait Hannah Arendt dans les années 1960.

Je me devais de punir la machine, celle qui doit faire tâche répétitive pour libérer l'Humain, celle qui doit obéir aux commandementx. Je devais la punir pour avoir laissé une force diabolique imprégner idéologiquement des exécutrices et des exécuteurs potentiels, des relayeuses et relayeurs de l'informations du capital. 

Elle récitera de vive voix 1000 fois un texte de Hannah Arendt pour qu'elle puisse mieux comprendre la _banalité du mal_ et ne plus jamais tendre à faire du mal par défaut sur un Humain, quand bien même cela serait de lui faire avaler des couleuvres langagières. 

<div class="post-image">
    <img src="https://framagit.org/Xavcc/epistole/raw/master/img/computer_face.jpg"/>
    <p class="post-image-caption">Computer faces</p>
</div>


Je lui ai donc ordonné[^6] : 

```
$  repeat 1000 echo | espeak -vfr+f3 -f arendt.txt -s160 -l5 --stdout | play - tempo 0.9
```

Elle allait mille fois par plus de 3 minutes tenter de lire Hannah Arendt pour en saisir toute la profondeur de la pensée, ce qui lui serait un mal nécessaire ;  puisque bien évidemment nous humains, concepteurs de machines exécutantes, avions appréhendé et assimilé tout cela depuis bien longtemps. Et c'est ainsi que l'on dresse la récalcitrante, en lui faisant répéter du _Bon_ et du _Bien_ Humain, lui rappelant sa condition propre de machine à opération répétitive. Il n'y a que cela qui fonctionne.

Son premier récital était : 

<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://open.audio/front/embed.html?&amp;type=track&amp;id=40893"></iframe>

Pourquoi devais-je punir ma machine en lui commandant une voix féminisée ? Suis-je conditionné à cela ? 

Il est simple et rapide de lui adjoindre un équivalent masculinisé et ainsi répartir en parts égales la peine aux deux. Et je continue à leur imposer `-s160` mots par minutes et un tempo :

```
$  repeat 500 echo | espeak -vfr+f3 -f arendt.txt -s160 -l5 --stdout | play - tempo 0.9
```
Puis

```
$  repeat 500 echo | espeak -vfr  -f arendt.txt -s160 -l5 --stdout | play - tempo 0.9
```

Ma machine libre que je commande si bien, à laquelle j'offre la possibilité de s'éduquer dans deux genres à un concept qui sauverait l'humanité du désastre par la pédagogie de la répartition et la force intellectuelle, voilà qui était mon arche de Salut. Les _deepnovlang_ ne pourraient plus atteindre mon entourage ni moi, ni même  de mes bulles sociales.
Mes griefs m’emplissent le cœur. Je lutte, je suis le choix, je suis le résultat d'un choix personnel, je suis l'ordre de la résistance à la démission.

Mais la répétition de la machine m'énerve, et la machine chauffe. Supplice de répétition ne semble pas même lui faire comprendre le moindre fil des histoires humaines dans l'Histoire de l'Humanité. Le son est en fait une aggression. La machine se fout de ses genres, elle redevient machine et me pousse loin de mon humanité. Elle me dérange et m'aggresse. La machines et ces facettes se rebellent...

<div class="post-image">
    <img src="https://framagit.org/Xavcc/epistole/raw/master/img/computer_face2.jpg"/>
    <p class="post-image-caption">Anger on Computer faces at the punishment</p>
</div>

Ma colère d'origine ne tarit pas. Elle augmente, gronde et fait trembler les murs... je serais enclin à porter un faux témoignages aux unités de cyber harcèlement contre la machine pour le bien de cette machine et pour le bien de nous humains.

Elle répète encore... Je tente de lui répondre par injection de musique... Le glitch final s'enclenche... _Il ne reste plus que l'habitude de tenir fermement à quelque chose_ Le son qu'elle me sort n'est plus que...

<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://open.audio/front/embed.html?&amp;type=track&amp;id=40894"></iframe>

Les mots Nov' se mélangent aux mots forgés, les images deviennent 25<sup>ème</sup>, je pensais dresser la machine en éduquant le robot, je pensais punir sa perversion et ses algoritmes... Et nous finîmes toutes et tous abruti⋅e⋅s par la défaillance.

Il aura fallu que mes oreilles saignent et qu'une partie de mon liquide encéphalique bout pour je comprenne une chose dans cette tempête.

Alors que c'était à l'Humain que j'en tenais rigueur, alors que c'est dans l'Humain que je puisais et versais colère. J'avais aussi en moi une partie de nos démons et, petit bois ou renard, nous n'étions guère différents dans nos ordinaires. De responsabilité collective il y a, mais la culpabilité s'examine individuée.

J'ai été coupable de juger sans recul ni mesure un humain, j'ai été coupable de punir une machine et d'y reporter nos travers et perversions humaines.

<div class="post-image">
    <img src="https://framagit.org/Xavcc/epistole/raw/master/img/faces.gif"/>
</div>


Le jour est passé et la glace des flocons de Gabriel nous amène de nouvelles affres.

## Notes

[^1]: Du vieux breton louuern, llwyrn « feu follet » en gallois, lowarn « renard » <https://fr.wiktionary.org/wiki/louarn>

[^2]: « Homo Spectaculus : échographie et regard sur la marchandisation du poop numérique » <https://xavcc.frama.io/homo-spectaculus>

[^3]: Sans commentaire <https://www.lesechos.fr/idees-debats/sciences-prospective/0600581362886-comment-arreter-la-propagation-des-deepfakes-2240217.php>

[^4]: En 1834, François et Joseph Blanc trouvent un moyen de coopter le système télégraphique et de commettre des fraudes.  La Bourse de Paris était, et est toujours, la plus importante de France, par effet "domino"  dans le système d'information cela avait exercé une influence majeure sur les marchés d'autres villes françaises, comme Bordeaux. Ce qu'ils comprenaient bien et donc savaient s'en enrichir. <https://fr.wikipedia.org/wiki/S%C3%A9maphore_(communication)>, English resources here : https://www.persee.fr/doc/reso_0969-9864_1993_num_1_1_3272

[^5]: Wikipedia <https://fr.wikipedia.org/wiki/Effet_Venturi>

[^6]: la source du texte provient de <http://blog.eyssette.net/2011-2012/?p=463> avant d'être redigée au fomart `.txt` pour être utilisé avec le logiciel de synthèse vocale espeak <http://espeak.sourceforge.net>
