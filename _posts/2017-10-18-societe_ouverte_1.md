---
layout: post
title: "Lettre à Arzhur"
date: 2017-10-10 14:05:14 +0100
location: Somewhere, France
---

Arzhur, 

[ta lettre](http://arthurmasson.xyz/societe-ouverte-lettre-a-xavier/) traitant de tes observations sur le milieu des praticiens de la fabrication open source et des philosophies de l'ouverture des données et des processus m'a beaucoup marquée.

Cela immédiatement fait penser à quelques lectures de Deleuze

> _Un philosophe, c'est pas quelqu'un qui contemple, et c'est même pas quelqu'un qui réfléchit. Un philosophe, c'est quelqu'un qui crée_

Dans l'utilisation et l'application des principes d'un société ouverte inspirée des mouvements Do It Yourself et open source, tu écris avoir réalisé qu’il est possible d’aller encore plus loin, "en l’appliquant à l’ensemble de la société. La production de biens et de services, les entreprises, l’éduction et la santé, mais aussi les textes de lois et la politique. Tu voudrais tendre " vers un "tout" qui pourraient être développé selon ces principes issues du monde libre.

<div class="post-image">
    <img src="https://farm5.static.flickr.com/4350/23481640378_f70df6a780_c.jpg" alt="A full-size image example" />
    <p class="post-image-caption">Erminig Gwenn St Michel de Braspart CC BY-NC-ND.</p>
</div>

Cela m'enthousiasme beaucoup, tout autant que cela me pose mille questions, pas toutes réjouissantes.

Tu écris "_l’individu reprendre tout son pouvoir_" dans ta lettre au sujet de cette société ouverte, mais sommes nous aujourd'hui déjà en capacité d'assumer un pouvoir dont l'ampleur nous dépasse parfois ? Un pouvoir de choix et de responsabilité colletcive ?  Les "puissants" lisent la sociologie de leur responsabilité avec les lunettes de leur habitus et certains trouveront un renforcement de leur racisme de classe dans la même description réaliste que d'autres soupçonneront d'être inspirée par le mépris de classe, si je paraphrase à pein Bourdieu.
"_Souriez vous êtes gérés, rigolez vous êtes “data surveillés”, pleurez vous êtes codés._" avais-je écris dans un texte lors [du Tour de France dans les communs en open source], avec une influence de Damasio, pour illuster cette abysse d'un pouvoir à assumer et les risque d'être dévoré par une artificialisation. le monde de l'ouverture porte en lui ses démons. Ceci pouvant être un sujet de lettre à part entière lors de nos prochians échanges.

Tzvetan Todorov à très écrit dans "Mémoire du mal, Tentation du bien" les risques de ceux qui fabriquent le droit d’ingérence par des "bombes humanitaires".
Nous devons prendre garde à ne pas chercher à imposer une vérité autocratique à d'autres sous prétexte que nous avons le trop vif souvenir d'un monde fermé qui aurait fait du mal. 

Dans de multpiles autres aspects je suis heureux de partager avec toi, et avec d'autres, cette impulsion de changment deparadigme pour tendre vers plus de transparence, d'équité, d'écologie et de liberté de faire.

<div class="post-image post-image--split">
    <img src="https://farm1.static.flickr.com/733/21387841506_d261bbbeef_c.jpg" alt="The first in an example of split-imagery" />
    <img src="https://farm1.static.flickr.com/719/21198017143_e68555b918_c.jpg" alt="The second in an example of split-imagery" />
</div>


Pour atteindre cette société ouverte il y a devant nous un gouffre à franchir, un désert à traverser ou même peut être un enfer à ne pas façonner. Pour cette raison, je crois que nous devrions nous appuyer sur ces personnes douées pour la fiction et l'anticipation par le récit ou les arts, afin d'intérroger l'objet même de cette société ouverte. Nous avons besoin, et peut être ont-elles besoin de nous en réciprocité, de personnes avec lesquelles travailler la matière de ce projet ; en manipulant les futurs possibles, et descomposants cette matière, afin déviter  que nous tentions d'en éviter les travers qui fabriqueraient des ennemis intimes de l'humanité.

Il me semble qu'Alain Damasio sera à Rennes prochainement. Pourquoi ne pas lui écrire à plusieurs une lettre manuscrite présentant le projet société ouverte et lui rédiger quelques questions, qui sont aussi des besoins que nous avons, afin d'avancer dans nos réflexions ? Nous pourrions aussi verser ces écrits dans un wiki avec une licence libre, histoire d'appliquer à nous même nos propres promesses ?

Au plaisir et avec impatience de te lire

_**Ksawery**_
