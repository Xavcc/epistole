---
layout: post
title:  "Nomades, nomadismes, Humanités…"
date:   2016-06-29 22:05:14 +0100
location: Glomel, France
---

Et si finalement le nomadisme post-moderne et les pratiques collaboratives étaient liés à une volonté de comprendre ce qui a fait de nous une Humanité ou des Humanités il y a des milliers d’années?

En voilà une bonne question à laquelle il ne suffira pas d’une copie double de 4h le cul sur une chaise pour répondre, ni d’un simple billet blog.
J’ai donc décidé de tenter différentes manières d’explorer les possibles pistes d’apprentissage dans ces questionnements. J’ai pris parti pour une aventure collective et des possibilités d’études collaboratives.
Répondre complètement à cette question me semble inepte surtout si le cheminement était réalisé sans pratiques collaboratives. Il m’est toujours apparu que le chemin était plus enrichissant que l’arrivée. Marchons et itérons ensemble alors, si vous le voulez bien.
Voici ce que je vous propose pour parcourir les monts et vaux des nomadismes et des Humanités.


<div class="post-image">
    <img src="https://framapic.org/Bkc7Kr5wPETl/uVBJo1YtstO4.jpeg" alt="A full-size image example" />
    <p class="post-image-caption">.</p>
</div>

## Collaborer pour chercher des réponses

La **connaissance** et le **savoir** sont des Communs, ils ne doivent pas être mis sous cloche à vide conservatoire ni être soumis à une privatisation brevetée mercantile. Si leurs libres circulations ont fluctué à travers les âges et l’Histoire en fonction des différents régimes de sociétés et climats politiques, il me semble éminemment vital et urgent au XXIème siècle d’appuyer ces deux ressources sur l’open source, la non centralisation et la distribution. Il est en enjeu majeur l’entretien et pollinisation de :

- . L’acquisition du savoir, accès au savoir, appropriation des savoirs ;

- . La transmission du savoir, échanges de savoirs, partage du savoir, circulation du savoir ;

- . La gestion du savoir, maîtrise des savoirs, valorisation des savoir.

**U**ne fois cette première brique de collaboration établie et acquise, il reste encore à concevoir la brique des interactions entre individus et/ou groupes de personnes. 
La bienveillance est évidement règle commune, une relation pair à pair est nécessaire afin d’alimenter un système de confiance sain et productif, la capacité d’apprenance de chacun doit être entretenue et cultivée, le droit à l’erreur est inaliénable et fait partie de l’apprentissage.
Je répète souvent qu’il n’y a pas de questions bêtes, les seules questions idiotes sont celles que l’on ne pose pas.
Pour résumer ces composants de la seconde brique de collaboration, je citerais:

-------------

> _[La loi de la mobilité ou loi des deux pieds](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert#M.C3.A9thode) : si vous n’êtes ni en train d’apprendre, ni de contribuer, passez à autre chose._

-------------

Voici donc deux petites briques que vont nous permettre d’aborder la grande question posée en introduction.

## Exploration, Introspection, Documentation

Nomadisme, post-modernisme, pratiques collaboratives, une Humanité ou des Humanités, cela fait quelques termes à approfondir, comprendre, définir dans une même question. C’est peut être là que réside la beauté poétique de ce challenge. 
Il n’en pas moins un enjeu important de s’atteler à ces approfondissements car j’ai la conviction que ce nous ne nommons pas ne peut pas exister et ce que nous nommons mal est aliéné. Pour tenter de comprendre des liens de causes à effets, comme énoncé en amorce de ce billet, mieux vaut explorer et se perdre dès le départ dans les méandres de nos origines de pensée et de conception. Une descente dans les abysses didactiques qui permettra une belle émergence d’une multitude de sources de valeurs.

------------

> _"Le nomadisme est un mode de vie fondé sur le déplacement ; il est par  conséquent un mode de peuplement matériel ou immatériel. La quête de  nourriture, sous toutes notions, motive les déplacements des humains.
Le nomade est celui qui se déplace. Il est celui qui peuple les  territoires, active les courants, insuffle la connaissance. C'est le  besoin de se nourrir des fruits de la terre et des graines des savoirs  qui le pousse à marcher le long des sentiers classiques dans dans les  lisères non battues"_

> _Extrait du dépôt sur GITHUB "[Nomades](http://www.multibao.org/#nomades/modus_nomades)"_

------------

**P**our cheminer collectivement sur la documentation du nomadisme, des pratiques collaboratives, je propose de participer à une documentation ouverte en open source que vous trouverez [ICI](https://github.com/XavCC/nomades). 
N’ayez pas peur de l’apparence austère de GITHUB, je vais tenter de vous expliquer simplement comment faire (et c’est facile) ainsi que les enjeux.


--------------

> _L’analyse de réseau ne transforme pas nos objets d’étude, elle transforme le regard que le chercheur porte sur ceux-ci. Organisée en réseau, l’information devient relationnelle. Elle rend possible en puissance la création d’une nouvelle connaissance, à l’image d’une encyclopédie dont les liens entre les notices tissent une toile dont on peut analyser les caractéristiques structurelles ou d’un répertoire d’archives qui voit sa hiérarchie bouleversée par un index qui recompose le réseau d’échange d’information à l’intérieur d’un groupe de personnes. Sur la base de deux exemples d’outils de gestion, conservation et valorisation de la connaissance, l’encyclopédie en ligne Wikipédia et les archives de la coopération intellectuelle de la Société des Nations, cet article questionne le rapport entre le chercheur et son objet compris dans sa globalité._

> _Grandjean, Martin (2014). [“La connaissance est un réseau”. Les Cahiers du Numérique](http://www.cairn.info/resume.php?ID_ARTICLE=LCN_103_0037)_

----------------



<div class="post-image">
    <img src="https://framapic.org/ABZ67TuFNodE/OfEIuvKqOn5h.png" alt="A full-size image example" />
    <p class="post-image-caption">.</p>
</div>


Tout d’abord Github doit être considéré comme une plateforme de collaboration et de documentation. Vous ne travaillez pas pour un autre sur sa ressource, nous travaillons ensemble sur une ressource commune. Je vous passe les détails techniques mais nous pourrions en discuter par visio-conférence si vous le souhaitez. Vous pouvez aussi croiser les avis avec Olivier Blondeau: [Les communs par l’exemple Github](http://www.makery.info/2016/06/25/les-communs-par-lexemple-github/), sur Makery.

--------------

> _Un très rapide et ludique entraînement avec huide d'utilisation vous est proposé sur MultiBao.org : [ICI](http://www.multibao.org/#multibao/documentation/blob/master/README.md)_

--------------


#### Explorer à partir des expérimentation de chacun

Le dépôt de documentation “[Nomades, nomadismes, Humanités](https://github.com/XavCC/nomades)” est à l’état de “draft” ou amorce de ressource commune. Je vous donne à le lire, réutiliser, copier et à y contribuer. 
Il est important que les personnes et organisations concernées par ce sujet s’emparent par elles-mêmes de la graine semée dans ce github. 
Imaginez une connaissance et des savoirs entretenus par des dizaines de points dans un réseau distribué et décentralisé, un Commun tout bleu mer pareil au schéma ci-dessus. Ce serait beau, ce serait puissant et nourrissant comme un océan. 
J’ai mis à disposition quelques écrits et documentations qui démarrent par un “readme” que vous pouvez lire et dont vous vous apercevez de l’état “draft”, car incomplet. Une invitation non dissimulée au voyage, une invitation à faire ensemble, à explorer ensemble les questions.

----------------

> _"La volonté de partage des pratiques collaboratives et la nécessité de documentation des expérimentations réalisées poussent la justification  de rendre visible le " comment faire ? " et le " bien faire ensemble "._

> _Le mode de vie du nomadisme repose sur le " bien vivre ensemble ". L'enjeu de mise dans les communs de ce bien vivre ensemble est une source d'alimentation de la transition qu'appelle une société en mutation. Les défis à résoudre concernant l'environnement, le capital  naturel, la nouvelle économie, la transmission des savoirs sont le pas fertile du nomade."_

> _Extrait du dépôt sur GITHUB_

-----------------

**L**es premiers écrits sont basés sur des collaborations avec des nomades, sur deux expériences intenses de nomadisme numérique en collaboration avec plusieurs dizaines de structures (Bretagne Lab Tour & LabOSe par exemple), ainsi que sur un mode de vie en itinérance choisie afin d’explorer et introspecter directement sur le terrain les chemins qui mène à mon questionnement premier sur les Nomades et les Humanités.
Je vous invite à mettre en commun nos différentes connaissances, ressources, savoirs. Je suis persuadé qu’au delà du numérique nous serons amenés à nous rencontrer pour des dialogues passionnants et des introspections pleines d’Humanités.

#### Par où et par quoi commencer?
Essayons de faire simple, ce qui est parfois complexe. Tout d’abord il s’agit de s’intéresser au README de ce dépôt “Nomades, nomadismes, Humanités. Disons qu’avec le document expliquant la licence d’utilisation, ce “readme” est la base, non figée, du travail collaboratif à venir. Chaque lecteur et contributeur doit pouvoir s’y référer pour comprendre le contexte, les cheminements et les objectifs du dépôt “Nomades” ainsi que des différentes fiches qui le composeront au fur et à mesure.


-----------

> _"Notre survie dépend bien plus de l’implication symbiotique élevée des organismes que de l’adaptation des individus par conditionnement.Plus la symbiose dans un écosystème est importante, plus une espèce à des chances de perdurer. Partager, Passer, Offrir… ceci est un principe simple de survie de l’espèce, de notre espèce humaine._

> _Les nomades sont des arpenteurs d'espaces physiques et de dimensions  immatérielles. Ils sont des vecteurs de pratiques et d’expériences et ils ont ainsi l'opportunité d'être utilisateur des pratiques collaboratives. Ils ont une forme de responsabilité dans le partage et le transmission de ces pratiques collaboratives "_

> _Extrait du dépôt sur GITHUB_

-----------

**Écrire et documenter collaborativement sur le README par github, les question en cours:**

- . Que doit contenir le ReadME? Qu’est-ce qui ne doit pas figuré dedans ?

- . Suggérez des modifications, contribuer au définitions, définir les Les différents ensembles sociaux et culturels de nomades

- . Le nomadisme à l’âge des internets serait à revoir et à étoffer

- . Les langages des nomades à l’ère du numérique à étoffer

- . La listes des ressources serait à alimenter

- . Corrections des coquilles et mise en page des textes

_NDLR: Petite raison supplémentaire de tenter de s’acculturer à la documentation et écriture collaborative sur github ; vous utilisez un langage appelé [markdown](https://fr.wikipedia.org/wiki/Markdown) qui est commun à github, Authorea, wikipedia, hackpad, Medium… vous monterez donc en compétences._

En exemple de liste proposée à la définition dans le dépôt github à laquelle vous pouvez participer:
**Les différents ensembles sociaux et culturels de nomades**

- . Les voyageurs

- . Les pollinisateurs

- . Les aventuriers

- . Les réfugiers

- . Les Makers

- . Semi nomadisme

- . Nomade permanent

- . Gens du voyage

- . Nomades de la mer

- . Nomade du sel

- . Caravane (véhicule)

- . Gitans, Roms

- . Campers

- . Travellers

- . Bergers, éleveurs,  transhumance

- . Nomadisme numérique

## Pour aller plus loin et prévoir la suite ensemble

Vous devez comprendre qu’à tout moment vous pouvez “Forker”, c’est à dire réutiliser, modifier et redistribuer, une partie ou l’ensemble du travail, si vous respectez les règles d’utilisations inhérentes à licence sous laquelle sont enregistrés les dépôts. Cette possibilité de fork est à votre bon gré et transparente. Elle est même nécessaire à la vie de la ressource produite.

Collaborer sur de ressources qui ne sont pas les vôtres avec cet outil, github, permet à la fois d’historiciser la production, de générer un bien commun informationnel, de cocréer un régime de propriété non exclusif – lire, comprendre, réutiliser, etc. – et de structurer une cogouvernance des ces biens. C'est donc vous approprier une partie de ces ressources et des responsabilités inhérentes, c'est recevoir autant que l'on partage (voir parfois plus).

Lorsque vous regarderez de plus près l’ensemble des fiches, vous verrez des suggestions qui n’attendent que vos plumes, réflexions, explorations et ressources, elles se nomment :

* . [Librairie de ressources](https://github.com/nomades/modus_nomades)

* . [Numérique](https://github.com/nomades/modus_nomades)

* . [Vivre nomade](https://github.com/nomades/modus_nomades)

**Il** vous suffit de cliquer dessus pour y accéder et y participer.
Vous pouvez rajouter d’autres fiches ressources, suggérer une nouvelle organisation.
Il n’y a rien de figé ni d’obligatoire, il s’agit d’invitation à la réflexion et documentation collective sur des premières idées de thèmes permettant de cheminer dans la question “Et si finalement le nomadisme post-moderne et les pratiques collaboratives étaient liés à la volonté de comprendre ce qui a fait de nous une Humanité ou des Humanités il y a des milliers d’années?”

-------

> _"La notion de travail collaboratif (peer production en anglais) n'est  plus fondé sur l'organisation hiérarchisée traditionnelle. S'est ouvert  avec l'évolution numérique un nouveau mode de travail et de nomadisme où collaborent de nombreuses personnes grâce aux technologies de l'information et de la communication, notamment les plates-formes internets "_

> _Extrait du dépôt sur GITHUB_

-------

_NDLR: Lorsque vous faites une modification sur les documents, écrits ou codes, il apparait une fenêtre “Commit changes”. Vous devez vraiment la renseigner pour que tout le monde puisse comprendre votre proposition de modification ou correction: 1/ titre de votre action ; 2/ détails et contexte de votre action_

<div class="post-image">
    <img src="https://framapic.org/iZ7gqmEgYIE2/TPtV00KA8XzZ.jpeg" alt="A full-size image example" />
    <p class="post-image-caption">Détail de nomadisme, Xavier Coadic sur les toits du port du Moros à Concarneau. Crédit photo: Emmanuel Poisson Quinton</p>
</div>


Une suite que j’envisage sous peu, après consolidation des ressources et documentation sur la grande question posées, est de créer une organisation dans multibao.org afin d’y déposer notre travail collaboratif pour partager nos expériences et savoirs avec d’autres cultures. Un partage des ressources produites avec movilab me parait également intéressant. Vos suggestions seront pour sûr les bienvenues.

**D**éjà écris précédemment mais j’insiste: aller plus loin et prévoir la suite ensemble signifie également provoquer des rencontres off-line. Je vous assure que je vais au bout de mes insistances avec bienveillance et curiosité. Je m’attacherais à faciliter et co-organiser les rencontres entre les différents contributeurs, je prendrais soin d’y participer les plus souvent possible.
Je ne pense pas être le seul à m’interroger sur l’Humanité et les Humanités. Des rencontres avec [Nicolas Thely](http://perso.univ-rennes2.fr/nicolas.thely) (Professeur des universités en art, esthétique et humanités numériques), [Yasir Siddiqui](http://www.sensorica.co/home/about-us/yasir-siddiqui)(strategy and innovation specialist), [Isabelle Delannoy](http://www.economie-symbiotique.fr/isabelle-delannoy/) (ingénieure agronome, auteure et conférencière, spécialisée dans l’économie symbiotique) ou encore tous les hôtes que j’ai croisés depuis 4 ans en étant semi ou totalement nomade, alimentent ma soif d’apprendre, d’explorer et de partager sur ces univers du nomadisme, des pratiques collaboratives et des Humanités.

Il ne tient qu’à votre bon gré d’embarquer dans ces explorations et documentations.

-------

> _J’espère vous avoir donné envie de prendre le voyage ainsi nous pourrions construire ensemble des navires pour passer d’univers en univers et polliniser par delà les frontières connues._

-------

<div class="post-image">
    <img src="https://framapic.org/OdCfiVNeLA6Y/OYt8jxQ7fSml.png" alt="A full-size image example" />
    <p class="post-image-caption">Detail from a map of Ortelius — Magellan’s ship, premier navire à avoir accompli la circumnavigation du globe This is a file from the Wikimedia Commons</p>
</div>


