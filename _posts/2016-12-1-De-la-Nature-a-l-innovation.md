---
layout: post
title:  "De la Nature à L'innovation par miracle de la beauté"
date:   2016-12-13 13:05:14 +0100
categories: biomimicry
location: Paris, France
---

Je voudrais vous faire voyager dans une petite prose de nature.
La nature et le vivant, une histoire longue de presque 4 milliard d'années qui se rappelle à nous les humains par de petits instants d’émerveillement. 

Simple, poétique, une expérience sensorielle comme s'allonger dans l'herbe et observer les fleurs pour ressentir l'esthétique et le sentiment de satisfaction d'être là. Tout simplement là où quelque chose de Commun exprime une formidable ingéniosité qui a traversé les âges.

3,8 milliards d'années d'innovation par la Nature, cela propose des observations infinies.
Si vous le voulez bien, parcourons 3 exemples de l'esthétique de la Nature.

< >

## En premier pas de ce voyage...

Les souffles du vent ou d'un enfant sur les aigrettes d'un pissenlit, j'ai toujours trouvé cela merveilleux. J'ai passé des heures et des heures depuis tout petit à sourire face à ce spectacle.  

<div class="post-image post-image--split">
    <img src="https://hackpad-attachments.imgix.net/hackpad.com_jmyQ3tCFPz0_p.266912_1478277349983_giphypissenlit.gif?fit=max&w=882" alt="The first in an example of split-imagery" />
    <img src="https://hackpad-attachments.imgix.net/hackpad.com_jmyQ3tCFPz0_p.266912_1478252554534_Sea_of_dandelions.jpg?fit=max&w=882" alt="The second in an example of split-imagery" />
</div>

<div class="post-image">
    <img src="https://c2.staticflickr.com/8/7297/16517340751_63a4e03d74_b.jpg" alt="A full-size image example" />
    <p class="post-image-caption">Dandelion Siphonophore | by NOAA Ocean Explorer.</p>
</div>

Ce végétal au dents de lion, souvent anodin aux yeux du plébéién, pousse inlassablement et se propage au gré des vents, graines nomades d'une harmonie bien orchestrée.      

Tant banal et si singulier, je l'ai cueilli mille fois.           

La sève blanche du pissenlit, celle qui colle au doigt lorsqu'on coupe le fragile végétal, est du latex. Du latex coule dans le pissenlit. Du latex naturel qui pourrait nous permettre de remplacer des milliers de produits pétro-sourcés par un caoutchouc naturel avec une ressource renouvelable et locale.
Un latex qui pourrait nous permettre de produire des préservatifs sans perturbateurs endocriniens ou des gants de chirurgie non allergènes. 

< >

## Dans la suite de ce voyage... 

A travers la Nature, les animaux peuvent également nous surprendre.
La nature est beauté, elle nous offre une esthétique à contempler pour nous chuchoter les solutions à de nombreux défis de nos humanités.

Jusque dans l'hostilité des déserts la vie a su trouver des voies d'adaptation. A l'époque du carbonifère, longtemps avant l’apparition des humains, il y a environ 350 millions d'années, apparaissaient les scorpions dans un temps où s'accumulaient de vastes couches de charbon en Europe de l'Ouest.

<div class="post-image post-image--split">
    <img src="https://hackpad-attachments.imgix.net/hackpad.com_jmyQ3tCFPz0_p.266912_1478276500739_scorpion.jpeg.jpg?fit=max&w=882" alt="The first in an example of split-imagery" />
    <img src="https://hackpad-attachments.imgix.net/hackpad.com_jmyQ3tCFPz0_p.266912_1478254593516_giant-hairy-scorpion.jpg?fit=max&w=882" alt="The second in an example of split-imagery" />
    <p class="post-image-caption">Scorpion sous la lumière noire.</p>
</div>

Alors que nous avons intensément utilisé ce charbon, à la fois source d’énergie mais malheureusement aussi de pollution, la petite bête, elle, a continué son chemin à travers les âges sans faire de déchets en résistant à des conditions extrêmes de température, d'abrasion du sable, des vents... La forme et la matière de son exosquelette pourraient nous être écologiques, ingénieuses et utiles.

Nous parlons là de matière. La chitine, il y en a de vos cheveux, associée à du carbonate de calcium ,la craie, les coquillage en sont fait, c'est un peu le cocktail de l'exosquelette du scorpion. Cela participe à sa résistances aux des conditions extrêmes de température basses ou hautes, d'abrasion du sable lors de tempête.

Conjuguons cela aux propriétés de structure caractéristiques de cette animal très ancien pour servir de base pour des habitats par exemple. Des constructions répondant aux même résistances faces aux conditions difficiles et aux mêmes exigences de frugalités environnementales que le scorpion.
Réguler la température du logis sans machine, se prémunir du chaud du désert, du froid des nuits, ne pas être abîmés par les tempête de sable grâce à l'exemple et l’étude du scorpion.

Pour bâtir rapidement des abris pour les réfugiés en zone aride avec des conditions d'habitat souhaitables, des performances énergétiques élevées et une exigence écologique.

Si l'on rajoute à cela l'émerveillement de la lumière... la cuticule des scorpions qui constitue leur exosquelette a la particularité d'être fluorescente en lumière noire. Je vous laisse imaginer les applications...
Nous aurions alors une nouvelle occasion de faire valoir notre humilité face à l’ingéniosité de la nature et les solutions qu'elle nous propose.

< >

## Quittons le monde du visible... 

Pour nous tourner vers celui de l'invisible à l'oeil nu : la nature recèle de bien nombreuses merveilles dissimulées tout autour de nous. 
Toujours habité d'un regard d'enfant, je m'extasie souvent sur le monde de bactéries.
Présentes absolument dans tous les milieux de notre planète, elles représentent la majorité de la biomasse.  

Il y a environ 40 millions de cellules bactériennes dans un gramme de sol et 1 million de cellules bactériennes dans un millilitre d’eau douce.

<div class="post-image post-image--split">
    <img src="https://framapic.org/5EfEB1d0dYzu/6H1KkUSWtWXR.jpg" alt="The first in an example of split-imagery" />
    <img src="https://pbs.twimg.com/media/CkB9xcIWgAAvP6u.jpg" alt="The second in an example of split-imagery" />
</div>

<div class="post-image">
    <img src="https://hackpad-attachments.imgix.net/hackpad.com_jmyQ3tCFPz0_p.266912_1478257390295_bacteria%20gif.gif?fit=max&w=882" alt="A full-size image example" />
    <p class="post-image-caption">Bacteria.</p>
</div>

Lorsqu'elles sont en symbiose avec des levures par exemples, nous pouvons les utiliser pour faire pousser nos textiles par le développement naturelle d'une couche de fibres celluloses bactiérennes, sans déchets, sans polluants, avec très peu d'énergie. Depuis votre propre garage et par vous même. 

D'autant plus curieux et poétiques que sont ces micro-organismes, ils nous permettent également par leur étude de concevoir et penser le système complexe des "villes intelligentes".
Pour aborder des problématiques sur les architectures liées à l'énergie, la santé, la mobilité ou les nouvelles technologies.

Un travail, et un grand plaisir, d'émerveillement que l'on peut réaliser avec plusieurs colonnes transparentes d'1 mètre de hauteur. Une colonne de [Winogradsky](https://en.wikipedia.org/wiki/Winogradsky_column) contenant 7 couches de différentes terres dont l'activité bactérienne spécifique à chaque couche nourrit ses strates voisines, toujours sans déchets. S'inspirer des Biomes bactériens pour optimiser les flux de matière, d'énergie et d'information.

S'inspirer des Biomes bactériens pour optimiser les flux de matière, d'énergie et d'information...

Ce dispositif éclaira des lignes de Led disposées au sol de jardin public pour donner à s'émerveiller et s'intéresser au plus grand
Une création artistique provenant des sols conjuguée à une technologie humaine dite de "smart contract" pour échanger en pair à pair des données environnementales et donner un medium d'expression à la micro flore des sols.

Pourquoi ? 

Pour réconcilier l'Humain et la Nature, resynchroniser technosphère et biosphère

* Donner une forme d'expression à des représentants du plus important règne du vivant : les bactéries et les micro-algues

* Rendre esthétique et contemplable ces représentants du Vivant. **Car l'on aime que ce que l'on connait et l'on protège ce que l'on aime.**

* Utiliser et enrichir une technologie émergente et acculturer à ses implications sociales . Coder ou être codés pour un humain est enjeu majeur du numérique.

 * Enclencher un programme de sciences citoyennes par delà les frontières. Biologie, SHS, recherche en design, énergie, urbanisme.
 
 * Co-concevoir des briques de systèmes énergétiques autonomes acentralisés biosourcés et préparer une économie de l'après pétrole
 
< >

## A travers cette petite histoire de la Nature et du vivant...

Je vous pose un question.

Qui n’a jamais rêvé d’être une exploratrice ou un explorateur d’espaces lointains, du centre de l’Australie ou d’une forêt près de chez soi ? 

Découvrir de nouvelles dimensions et engager des dialogues avec des formes de vie plus que surprenantes. Se perdre et se retrouver dans l’immensité poétique d’objets lumineux de contemplation.

---

>  _L’exploration serait le fait de chercher avec l’intention de découvrir quelque chose d’inconnu, un mot provenant du latin "exploratio" : observation, examen._

---

Ainsi d'un regard d'enfant qui perdure encore aujourd'hui, j'observe les petites choses de la vie pour m'en réjouir de leur beauté parfois anodine qui provient de manifestations telles que la forme, la matière ou le système exprimés dans la nature.

S'inspirer, imiter, copier ces innovations du Vivant cela s’appelle du biomimétisme. 

<div class="post-image">
    <img src="https://hackpad-attachments.imgix.net/hackpad.com_jmyQ3tCFPz0_p.266912_1478959728672_bhhp_1_original(1).png?fit=max&w=882" alt="A full-size image example" />
    <p class="post-image-caption">Le Biome, Biomimicry HackLab.</p>
</div>

Faire cela pour résoudre des défis sociétaux tout en préservant la biodiversité et en réinvestissant dans la capital naturel.
Concevoir et prototyper des formes, des matériaux, des systèmes inspirés du vivant, respectueux des cycles de la nature, avec une durabilité et une progression sociale. C'est le Biomimétisme.

Faire mieux avec moins. Comme la nature optimiser les flux de matière, d'énergie et d'information. Réconcilier les activités humaines et la biosphère.

Retrouver un équilibre et une sagesse avec notre propre humanité. Cela passe selon moi par notre curiosité sur le monde qui nous entoure, par la connaissance de la nature, par son amour qui mène à sa protection. 

Dans un petit instant d'émerveillement pour faire renaître un grand changement.

---

> _“Innovare” signifie « revenir à, renouveler ». Ce mot se  compose du verbe novare et de la racine novus, qui veut dire  « changer », « nouveau », et du préfixe in-, qui indique un mouvement  vers l’intérieur_

> _L’innovation ne peut être confondue avec l’invention, qui propose ce qui n’existait pas._

> _L’innovation part de l’existant qu’elle réorganise dans sa structure interne._

---

Cette histoire de la Nature et la prose du Vivant nous amène à un projet plus grand que les objets et les solution décrites.
Le vivant c'est la symbiose et la collaboration, l'intelligence collective, la coopétition...

D'après les travaux d'[Eric Kasenti](https://fr.wikipedia.org/wiki/%C3%89ric_Karsenti), la survie dépend bien plus de l’implication symbiotique élevée des organismes que de l’adaptation aux conditions par conditionnement. Plus la symbiose dans un écosystème est importante, plus une espèce à des  chances de perdurer. 

Partager, Passer, Offrir… ceci est un principe  simple de survie de l’espèce, de notre espèce humaine.

Pour faire cela nous avons des Communs. La biodiversité, l'eau, l'air, les savoirs et la connaissance... correspondent à un ensemble de ressources. 

<div class="post-image">
    <img src="https://framapic.org/leahtimtw6XE/qYxEAl5uidff.png" alt="A full-size image example" />
    <p class="post-image-caption">Le Biome, Biomimicry HackLab.</p>
</div>

Mais Les communs ne sont pas juste une ressource. C’est une ressource plus une communauté, plus ses protocoles sociaux et ses valeurs pour gérer les ressources partagées. Les communs sont un paradigme socio-économique. C’est un système social pour la coproduction et la co-gouvernance.

Traiter un bien commun comme un bien privé conduit à sa destruction, comme l'a souligné [Garrett Hardin](https://fr.wikipedia.org/wiki/Garrett_Hardin).

La forêt appose t-elle des brevets sur son ingéniosité et ses productions ? Ne devons-nous pas aller dans le sens du biomimétisme pour une libre circulation de la connaissance et des savoir-faire ? 

A l'age des internets, nous avons la possibilité et la responsabilité de concevoir un nouveau paradigme de société et de préserver la biosphère. Il en va de notre survie d'espèce comme du notre devoir envers nos enfants.

Pour faire cela j'ai co-crée un biomimétisme HackLab. Un laboratoire citoyen de prototypage et de solutions en open source inspirées du vivant avec une communauté d'acteurs libres qui échangent, cultivent et entretiennent des ressources.

La sève de pissenlit, l’abri inspiré du scorpion, mais sans la fluorescence je le concède, les objets technologiques à partir des bactéries, des échanges de graines, sont des exemples de ce que nous faisons. Design, code informatique, architecture, biologie, philosophie, botanique, high tech, low tech, nos pratiques sont collaboratives, horizontales et notre légitimité se base sur le Faire et non plus sur un diplôme. Tout le monde peu apprendre en pratiquant.

Je ne suis pas vraiment un scientifique à proprement parler. Je suis votre voisin, votre covoitureur, votre inconnu dans la rue, un type qui fait des petits bouts de trucs et qui les assemble ensemble. Parce que l'information et la connaissance n'ont jamais été aussi accessibles qu'aujourd'hui.

Des collaborations et des échanges distribués et décentralisés par delà les frontières rendus possibles par internet, un peu comme le réseau de la nature.

< >

Pour faire cela j'ai dormi sous des ponts, parfois sans manger. 
En début d'année j'avais plaqué mes activités professionnelles avec juste un sac à dos et un ordinateur pendant 4 mois pour faire un tour de France aux services d'autres projets open source qui œuvrent dans les Communs. En ne vivant que des dons que faisaient des citoyens par internet chaque jour de travail accompli. En comptant sur la confiance des mes hôtes. Un peu comme un pollinisateur dans un champs d'innovations émergentes.

<div class="post-image">
    <img src="https://framapic.org/MM6oSmCpaiqk/osa1iaWLe3lM.png" alt="A full-size image example" />
    <p class="post-image-caption">Texte qui a servi de base pour mon intervention au TedX EM Lyon 2016. Pour leur aide salutaire, MERCI Cammille Fourre, Victoria Adjanohoun, Madeleine Gancel </p>
</div>

Alors je peux bien intervenir comme prof ou mentor dans de belles écoles ou des universités. Créer des start-ups ou accompagner des grandes entreprises, mais cela n'a aucune valeur ni forme d'importance si je suis privé de la chose simple, poétique, d'une expérience comme s'allonger dans l'herbe et observer les fleurs autour, si je suis privé de l'accès à sa connaissance, si je suis privé de la richesse du Vivant.

Nous avons besoin de nous émerveiller de la nature pour quelle devienne un bien Commun de l'humanité.
En commençant par les petites choses de notre quotidien, nous avons besoin d'écrire notre poésie avec la nature pour construire un avenir soutenable, désirable et durable.

<iframe width="560" height="315" src="https://www.youtube.com/embed/JQ68y_725Dw" frameborder="0" allowfullscreen></iframe>
