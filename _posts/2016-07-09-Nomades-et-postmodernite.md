---
layout: post
title:  "Lier le nomadisme postmoderne à une volonté de comprendre ce qui a fait de nous une Humanité il y a des milliers d’années ?"
date:   2016-07-09 13:05:14 +0100
location: Rennes, France
---

<div class="post-image">
    <img src="https://cdn-images-1.medium.com/max/2000/1*R_OhNqZWcJi_Ni-bV2xCtA.jpeg" alt="A full-size image example" />
    <p class="post-image-caption">BA — voyage- Source flickr.com Author ~ ℬoudicca — Attribution-Non Commercial License</p>
</div>
 

Vaste question que je pose là, grand sujet d’interrogation et
d’exploration qui peut faire fuir le lecteur. Belle promesse de voyages
et de poésies qui souffle une tentation à la facilité superficielle de
considération du champ exploratoire pour l’auteur.

**P**our se lancer à l’abordage d’un tel vaisseau d’exploration j’essaie de
ne pas me contenter d’un congédiement de la pensée critique, je tente de
préserver une humilité complexe.

> “Le but de la recherche de méthode n’est pas de trouver un principe
> unitaire de toute connaissance, mais d’indiquer les émergences d’une
> pensée complexe, qui ne se réduit ni à la science, ni à la
> philosophie, mais qui permet leur intercommunication en opérant des
> boucles dialogiques.”
> Edgar Morin, *Science avec conscience* (1982)

Depuis 2013, je me suis nourri de plusieurs expérimentations itinérantes
ou nomades personnelles et collectives. J’ai navigué, marché, couru,
grimpé, chuté, dans différents chemins du nomadisme. Prendre le temps de
jardiner ces champs, observer les cultures, contempler les natures,
récolter et digérer les graines et fruits puis recommencer à cultiver.
Prendre ces temps était le moindre des respects à porter aux sujets
abordés.

**P**artager ces interrogations, ainsi que les observations
glanées, avec de nombreuses personnes, écoles et organisations, est un
minimum vital pour la diversité nécessaire à nos champs de réflexions. 
Depuis un exercice de terrain en octobre 2015, où j’ai vécu en
exploration sur la naissance des interactions de confiances ente
individus et entre groupes de personnes, les travaux et interrogations
se sont intensifiés.
Suite à ce mois intenses je n’ai cessé de plonger toujours plus profond dans les questionnements limbiques. L’accessibilité de la connaissance à l’âge des internets, le partage des savoirs à l’ère de la mobilité, les
Humanités numériques, les pratiques collaboratives, la pollinisation des
multitudes… autant de raisons de se mettre en action et recherchecollaborative. 
Usages des réseaux numériques, ouverture appuyée par des plateformes contributives
(ex: doc [Github](https://github.com/nomades)), croisement des disciplines en présentielle, online hangout transatlantique, conférences et ateliers… 

Tout cela pour revenir aux basiques: **ce que nous ne nommons pas ne
peut pas exister, ce que nous nommons mal est aliéné**. 
Tout cela pour relire les écrits de Pascal Picq ([Histoire de l’homme et
changements climatiques](http://www.fayard.fr/histoire-de-lhomme-et-changements-climatiques-9782213628721), [Aux Origines de l’humanité,tome](http://www.fayard.fr/aux-origines-de-lhumanite-tome-2-9782213603704)1 et 2). 

Toute expédition commence par un premier petit pas et une première page.

> L’ **humanité** est à la fois l’ensemble des [individus](https://fr.wikipedia.org/wiki/Individu "Individu") appartenants à
> l’[espèce](https://fr.wikipedia.org/wiki/Esp%C3%A8ce "Espèce") humaine ([*Homosapiens*](https://fr.wikipedia.org/wiki/Homo_sapiens "Homo sapiens") mais aussi les caractéristiques
> particulières qui définissent l’appartenance à cet ensemble.

> L’ humanité réunit aussi certains des traits de [personnalité](https://fr.wikipedia.org/wiki/Personnalit%C3%A9 "Personnalité")d’un individu qui, par exemple, amplifient
> les qualités ou les valeurs considérées comme essentielles à l’humain, telles que la [bonté](https://fr.wikipedia.org/wiki/Bont%C3%A9 "Bonté"), la [générosité](https://fr.wikipedia.org/wiki/Altruisme "Altruisme")

> Le concept d’humanité est aussi à rapprocher de la notion de **nature
> humaine** qui souligne l’idée que les êtres humains ont en commun
> certaines caractéristiques essentielles, une
> [nature](https://fr.wikipedia.org/wiki/Nature "Nature")limitée et des comportements spécifiques.
> Ce qui les différencie des autres espèces animales 
> (Wikipédia)

**P**our aborder un telle question, il ne s’agissait pas
uniquement de rester derrière un ordinateur à écumer des espaces de
coworking ni de partir sur les routes d’un pays lointain avec un sac à
dos pour seul compagnon et dans les deux cas partager ses cartes
postales ethnocentrées dans une analyse niveau magazine de plage. J’ai
essayé de le faire et c’est profondément chiant et intellectuellement
polluant. 
Il vit dans ces questions sociétales un peu plus de complexité et ils
s’intriquent un peu plus d’enjeux dans les réponses possibles qu’un
simple personnal branding.
Tout d’abord je suggère de considérer qu’**il n’y a pas une forme de
nomadisme mais des Nomadismes avec différentes pratiques et ainsi de
multiples ensembles de Nomades**. Une suggestion que je propose de
considérer valable dans le temps actuel comme à travers les âges de
notre Histoire. 
Ensuite **les questions de l’Humanité, des Humanités, de leurs sources
premières et origines, me semblent être assez précieuses** pour ne pas
souffrir d’une vanité de traitement des sujets.

> « Le [propre de l’humain](https://fr.wikipedia.org/wiki/Humanit%C3%A9 "Humanité")n’est-il pas justement de se poser cette
> question : “Qu’est ce que l’humain ? » Et est-ce ce sens propre à
> notre espèce [*Homosapiens*](https://fr.wikipedia.org/wiki/Homo_sapiens "Homo sapiens")? Dans ce cas, les autres Hommes, dits
> préhistoriques, étaient-ils des humains ?”

> Passcal Picq répond à sa propre interrogation : “L’humain est bien une
> invention des Hommes, qui repose sur notre héritage évolutif partagé,
> mais n’est pas une évidence pour autant. Homo sapiens n’est pas humain
> de fait. Il a inventé l’humain et il lui reste à devenir humain, ce
> qui sera fait lorsqu’il regardera le monde qui l’entoure avec
> humanité”

Tenter d’alimenter en réponses possibles la question “ *Lier le
nomadisme postmoderne à une volonté de comprendre ce qui a fait de nous
une Humanité il y a des milliers d’années?*”, c’est s’efforcer à une
appréhension profonde des enjeux cités ; c’est comprendre les Nomadismes
pour arriver jusqu’aux pratiques postmodernes ; c’est chercher les bords
de L’ Humanité et les interstices entre les Humanités.

<iframe width="560" height="315" src="https://www.youtube.com/embed/EIkTcWcXclw" frameborder="0" allowfullscreen></iframe>

Il ne suffit pas de comparer un comportement socio-générationnel actuel
avec une hypothèse ethno-anthropologique sur nos lointain ancêtres pour
justifier une liaison directe entre état du travail en 2016 et des
migrations datant d’il y a 200 000 ans. 
Le risque d’amalgame pris parfois entre des européens équipés d’un
ordinateur portable et les peuples tels que par exemple les
[Sentinelles](https://fr.wikipedia.org/wiki/Sentinelles_%28peuple%29),
[Mani](https://fr.wikipedia.org/wiki/Mani_%28peuple%29),[Kintaq,](https://fr.wikipedia.org/wiki/Kintaq)me semble être une vision légère et européano-centrée qui flirte avec l’affront à ce qui pourrait faire de nous une Humanité, affront à l’apprentissage social et de l’acquisition de comportements à partir de l’observation dans le respect d’autrui.
Le savoir devenant dès lors une simple “marchandise informationnelle”
par abandon de l’étude et de la pensée critique à des schémas narratifs
visant l’explication d’une forme d’intégralité de l’histoire humaine, de
l’expérience et de la connaissance. J’ai mal à **La Condition
postmoderne. Rapport sur le savoir** de [Jean-François Lyotard](https://fr.wikipedia.org/wiki/Jean-Fran%C3%A7ois_Lyotard "Jean-François Lyotard"), j’ai mal à la [**philosophie postmoderne**](https://fr.wikipedia.org/wiki/Philosophie_postmoderne "Philosophie postmoderne").

<iframe width="560" height="315" src="https://www.youtube.com/embed/2MeC5uC48Kk" frameborder="0" allowfullscreen></iframe>

Choisissons des vaisseaux exploratoires et des voies de navigations plus
adaptés aux enjeux traités et ne cédons pas aux sirènes du marketing des
mots. La question posée en titre en vaut bien l'effort.

> Le nomadisme est un mode de vie fondé sur le déplacement ; il est par
> conséquent un mode de peuplement matériel ou immatériel. La quête de
> nourriture, sous toutes notions matérielles ou immatérielles, motive
> les déplacements des humains.

> Le nomade est celui qui se déplace. Il est celui qui peuple les
> territoires, active les courants, insuffle la connaissance. C’est le
> besoin de se nourrir des fruits de la terre et des graines des savoirs
> qui le pousse à marcher le long des sentiers classiques où dans les
> lisères non battues.
> ( [introduction du dépôt sur github)](https://github.com/nomades)

## Quelques voies de navigations exploratoires pour se lancer 

Évitons les voix des RH qui hurlent aux concepts de classifications
novateurs, si ces services et leurs considérations avaient de la
bienveillance et cherchaient l’innovation ils se transformeraient en
Richesse Humaines pour quitter le concept de Ressource Humaine
(ressource: que l’on exploite comme un mine ?).

Évitons les théorèmes marketing qui n’ont rien à voir avec les
nomadismes, les Humanités ou les univers numériques.

Choisissez et définissez vos propres voies de navigations en cultivant
votre propre créativité et en jardinant de vraie culture qui vous
correspondent. **Faites cela avec esprit critique et appropriation de la
connaissance, transmission des savoirs**.

1. Pour démarrer dans l’exploration de la question en titre, les
    **bingos à éviter individuellement voir à proscrire dans la même
    phrase** :

-  . **Génération X, Y, Z** *(vous n’êtes pas qu’une simple lettre choisie par du marketing)*
    
-  . **Multipotentiel** (*si un jour on vous à fait avaler que vous étes
    monopotentiel, on tente de vous asservir. Il est est profondément humain d’être multiple, le reste est du marketing*)

-  . **Coach en nomadisme, coach en agilité** (*vous vendre un produitqui n’existe pas pour satisfaire un besoin qui a été créé par le vendeur lui même ça marchait de 1980 à 1998, non?*)
    
- . **Slasheur 2.0 du web en open Hauts Potentiels** (*Bullshit bingo d’or à*
    [*Cadre Emploi*](http://www.cadremploi.fr/editorial/conseils/conseils-carriere/detail/article/comment-travaillent-les-slasheurs-2-0-pros-du-web.html)* qui vous considère comme un pur produit à consommer et se
    tape du nomadisme et des Humanités*)

Ces bingos ne sont pas fait pour vous apprendre la moindre chose ou vous
permettre quoique ce soit. Ils ne sont là que pour vous
“[encuber](https://medium.com/@XavierCoadic/le-logiciel-d%C3%A9vore-le-monde-marchons-dans-le-dehors-ff430601f8eb#.yngkqzf1n)”. Ces concepts ne répondent à aucune question essentielle ou existentielle.

2. Pour tracer des voies sans embuches, **éviter les récifs :**

-  . **Nomadisme = voyages = fuites de problèmes** psychologico-famillaux
    *(ineptie régulière, très inutile à débattre)*

-  . **Nomadisme moderne ou postmoderne = numérique** (*Il y aurait 250
    millions de réfugiés climatiques chaque année autour de 2050, des
    nomades contraints… Le numérique n’est pas encore écolo… Bref,
    inutile de débattre ce raccourci)*
    
-  . **Nomades = assistés** ( ¯\_(ツ)_/¯, *je capitule devant tant
    d’inepties…*)

Ces récifs sont fait que pour entailler la coque de votre navire de
volonté exploratoire. Ils n’ont pas d’autre utilité. Les personnes qui
les scandent sont des naufragés qu’il faudra revenir chercher un jour.

3. Pour **se rafraichir et s’acculturer** lors des navigations exploratoires :

-   . [**Sea is my Country**](http://www.balibari.com/films/sea-is-my-country/), film documentaire et expo transmedia du
    Nantais Marc Picavez.

-   .Le [sujet de Thèse de Malo Deplancke](http://www.youscribe.com/catalogue/tous/savoirs/science-de-la-nature/projet-these-amandier-2-541602) à travers lequel **les déplacements humains jouent sur la domestication et l’évolution de la diversité génétique des arbres fruitiers**. *{Je cherche les travaux de la thèse pour aller plus loin}*
    
-   . [**Toward a Critical Nomadism**](http://www.academia.edu/10258080/Toward_a_Critical_Nomadism_Felix_Guattari_in_Japan) ? Felix Guattari in Japan

-   . [**Hackbases,Nomadbases**](http://pad.totalism.org/p/alike),curated by [@dcht00](https://twitter.com/@dcht00) / [CHT Hackbase](http://totalism.org)

-   . [**Les nomades, nos voisins d’hier et aujourd’hui**: Dominiqu Careil at TEDxBordeaux](https://www.youtube.com/watch?v=EIkTcWcXclw "Les nomades, nos voisins d'hier et aujourd'hui: Dominique Careil at TEDxBordeaux") en vidéo

-   . [**Michel Maffesoli : Du nomadisme. Vagabondages initiatiques**](http://www.persee.fr/doc/agora_1268-5666_1997_num_10_1_1574_t12_0132_0000_2)

Cette liste est totalement arbitraire et bien trop non exhaustive. Elle
est complétée par une amorce Bibliothèque en fin de page. Je vous invite
à compléter ces listes.

<div class="post-image">
    <img src="https://cdn-images-1.medium.com/max/600/1*WxdG4_WWBm-pC8Jq2Js-Ag.jpeg" alt="A full-size image example" />
    <p class="post-image-caption">License: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication</p>
</div>


La boussole pour découvrir, comprendre et naviguer dans les questions de
nomadisme, vous l’avez en vous. Les modes d’emplois sont chez vos pairs
qui vous les partagerons, ils sont aussi en lignes dans les internets.
La boussole est rattachée à votre [pensée critique](https://fr.wikipedia.org/wiki/Pens%C3%A9e_critique) à votre curiosité.

<iframe width="560" height="315" src="https://www.youtube.com/embed/X55HCa2_BHI" frameborder="0" allowfullscreen></iframe>

Je ne vous ai donné que quelques points de carte pour naviguer dans les
univers des nomadismes et des humanités, c’est maintenant votre capacité
d’apprenance qui vous rendra autonome. C’est à vous et par vous même
d’arpenter les possibilités de réponses à vos besoins. Humains nomades,
itinérants, sédentaires, multiples, célébrez votre richesse Humaine.

> “Une chose en tout cas est certaine : c’est que l’Homme n’est pas le
> plus vieux problème ni le plus constant qui se soit posé au savoir
> humain. ”
> Michel Foucault

## Quelques champs à cultiver pour nourrir la réflexion 

*Le nomadisme postmoderne pratiqué avec une volonté de comprendre ce qui
a fait de nous une Humanité il y a des milliers d’années,* ceci n’est
pas une quête obligatoire. **Être nomade est un mode vie choisi ou subi
dont l’origine n’est pas intrinsèquement la conséquence de
questionnements**.(voir aussi "[Numérique et postmoderne : l’ère des flux](http://laurent.jullier.free.fr/TEL/LJ2009_Flussi.pdf))

La recherche de compréhension de ce qui a fait de nous une Humanité
n’est pas uniquement liée à des pratiques de nomadisme. Cette recherche
est si complexe et si profonde qu’elle ne saurait supporter une seule
approche parfois égoïste. J’avoue volontiers m’être posé la question
existentielle des nomadismes et des humanités lors d’introspections puis
d’avoir tenté d’embarquer du monde dans ma barque. Je songe à préserver
les questionnements de ce parasitisme. 

L’ **Humanité ne doit être confondue avec la condition humaine**. Cette
condition humaine est définie comme les caractéristiques, événements
majeurs et situations qui composent l’essentiel de l’existence humaine,
tels que la naissance, la croissance, l’aptitude à ressentir des
émotions ou à former des aspirations, le conflit, la mortalité. Être
dans un virage personnel ou professionnel qui nous mène sur une nouvelle
mobilité ou instabilité ne fait de nous des nomades qui se
retrouveraient aux origines de la nature humaine. Il me semble en
revanche plus juste de penser que parfois ces évènements marquants nous
rapproche un peu d’une condition humaine bien souvent oubliée dans un
monde très difficile et changeant.

> « L’homme a souvent été présenté comme le seul animal capable de
> planifier mentalement les actions nécessaires à la fabrication d’un
> outil. Cette affirmation repose sur l’idée que la fabrication d’un
> outil implique une faculté de planification mentale particulièrement
> élaborée. » 
> [Christophe Boesch](https://fr.wikipedia.org/wiki/Christophe_Boesch)

Aujourd’hui nous avons observé et compris que d’autres espèces animales
et même des machines avaient cette capacité. Il est ainsi un formidable
enjeu d’explorer ce qui fait de nous une Humanité et ce qui nous relie à
travers la planète et les âges.

**N**ous vivons une époque complexe composée de changements
vertigineux, de nouvelles opportunités, de dislocations brutales. Nous
naviguons entre promesses et espoirs, entre mensonges et désillusions,
entre détresses et craintes. 

Dans ce monde, je ne trouve pas cela complètement fou de rechercher ce
qui a fait de nous une Humanité, de tenter de comprendre les nomadismes
anciens, actuels et futurs dans une époque de mutation. 

J’y pressens de belles voies de redécouverte des socles essentiels à
notre bien-vivre ensemble lors des explorations des Humanités numériques
ou non numériques. J’espère trouver dans ces champs exploratoires
quelques pistes de réponses qui me permettront d’embrasser une liberté
réconciliée de l’Homme avec sa nature.

> Si le processus d’universalisation pousse les scientifiques à se déplacer, c’est en raison de la complexité du savoir. Ses éléments codifiés (équations, résultats d’expériences, etc.) peuvent se diffuser facilement, mais l’essentiel de la pratique nécessaire pour y aboutir en même temps que pour les reproduire et les appliquer à des fins spécifiques dépend d’un savoir tacite incarné dans des êtres humains. Leurs mouvements ne s’inscrivent pas nécessairement dans la longue durée. "

> [Nomadisme des scientifiques et nouvelle géopolitique du savoir](http://www.cairn.info/revue-internationale-des-sciences-sociales-2001-2-page-341.html)

<div class="post-image">
    <img src="https://cdn-images-1.medium.com/max/800/1*mRDrcABDgQfyv90gXKq24Q.jpeg" alt="A full-size image example" />
    <p class="post-image-caption">A Reality called Boom — Visions @ Boom 2014 Author Sterneck / photo on flickr — License CC BY-NC-SA</p>
</div>


**L**ire, entendre et voir des citoyens qui s’emparent de
questions de fonds avec le droit de débuter sans expérience, avec le
droit à l’erreur, avec le pouvoir d’apprendre, avec une citoyenneté qui
se cultive individuellement et collectivement, c’est ce que j’aimerais
au quotidien ordinaire. Libérer les savoirs et les actions dans une
Do-ocratie :

> “ If you want someting done 

> DO IT
>  But remember to

> BE EXCELLENT TO EACH OTHER
>  When doing so.”

> [Noisebridge Vision](https://www.noisebridge.net/wiki/Noisebridge_Vision)

Pas d’experts qui enferment les pratiques dans une normalisation
étouffant la création ; des enjeux sociétaux pris à bras le corps par
celles et ceux qui composent la société, voilà parfois les graines de
cette utopie que je sème et cultive ici et là au gré des déplacements. 

> La postmodernité fait référence à un changement structurel de l’individu et de la société lié à la fin de l’époque industrielle qui avait créé la modernité et à l’avènement de l’ère de l’information que nous connaissons aujourd’hui. D’après des sociologues comme Baudrillard (1970), Lyotard (1979) ou Maffesoli (1988), l’individu postmoderne serait né de l’effritement progressif des structures institutionnelles, sociales et spirituelles au sein de la société et d’une volonté de libération des dogmes, normes et valeurs traditionnelles. Le tout sur fond de crise socio-économique dans les années 1970 et 1980 qui a créé un désenchantement chez de nombreuses personnes. D’après Hetzel (2002), « la conception postmoderne de la société est en rupture idéologique avec les valeurs modernes de progrès, d’évolution vers un monde meilleur ou d’utopies collectives. 

> [Les paradoxes du consommateur postmoderne par Alain Decrop](https://www.cairn.info/revue-reflets-et-perspectives-de-la-vie-economique-2008-2-page-85.htm)

Saisissez-vous des ces questionnements et documentations sur les
nomadismes, les pratiques collaboratives, les Humanités. Prenez la barre
de vos vies et naviguez pour repenser la société.

**C’**est avec les collaborations en ligne comme avec les
œuvres réalisées physiquement en groupe que nous arrosons les champs qui
nous offrent les opportunités d’observer les cultures, contempler les
natures, récolter et digérer les graines et fruits puis de recommencer à
cultiver.

C’est dans ces pratiques collaboratives, au milieu de ces aires de
confiances, sur ces voies de vies croisées que se cachent les recettes
de nos Humanités.


