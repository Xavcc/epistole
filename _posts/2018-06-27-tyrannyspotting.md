---
layout: post
title:  "Tyranny's potting #PSES2018"
date:   2018-06-29 11:35:14 +0100
location: Rennes, France
---

Choisir le numérique digital, choisir un boulot digitalisé, choisir une carrière libérée de toute protection de l'intimité, choisir une putain de télé connectée à la con, choisir des machines IOT à laver, des bagnoles smarts, des playlists youtube, des smartphones android. Choisir la santé data drivée, un faible taux de cholestérol et une bonne mutuelle avec des trackers. Choisir les prêts à taux fixe avec AdWords, choisir son petit pavillon via Chrome, choisir ses amis via fichage et métadonnées, choisir ses vacances gentrifiées sur ok google, choisir son canapé avec les deux fauteuils sur une application du playstore, le tout à crédit de données personnelles avec un choix de tissus de merde. Choisir de bricoler le dimanche matin en s’interrogeant sur le sens de la vie, choisir de s’affaler sur ce putain de canapé et se lobotomiser aux jeux vidéo d'Intelliengence Artificielle pilotant des drones assassins en se bourrant de UberEat. Choisir de pourrir à l’hospice non financée par l'optimisation fiscale de google et de finir en se pissant dessus dans la misère en réalisant qu’on fait honte aux enfants niqués de la tête qu’on a pondus pour qu’ils prennent le relais de cette tyrannie. Choisir son avenir dans un atelier google, choisir la vie dans un nuage digitale. Pourquoi je ferais une chose pareille ? J’ai choisi de pas choisir la vie digitale, j’ai choisi autre chose. Les raisons ? Y a pas de raisons. On a pas besoin de raisons quand on a les libertés. 

+ Pas Sage En Seine 2018, vendredi 29 juin 2018 14h00 : <https://programme.passageenseine.fr>

+ « L'idiot du village g00gle » : <https://xavcc.frama.io/epistole/2018/03/20/google>

+ wiki `#nog00gle` : <https://no-google.frama.wiki>

+ `#fuckoffgoogle` : <https://fuckoffgoogle.de>

Choose digital digitalized, choose a digitalized job, choose a career free of privacy protection, choose a fucking asshole connected TV, choose IOT washing machines, smarts cars, youtube playlists, smartphones android. Choose drived data health, low cholesterol and a good mutual with data trackers. Choose fixed rate loans with AdWords, choose your little pavilion via Chrome, choose your friends via spying and metadata, choose your nice holidays on ok google, choose your sofa with the two armchairs on a playstore application, all on credit of personal data with a choice of shit fabrics. Choose to tinker on Sunday morning while wondering about the meaning of life, choose to slouch on this fucking couch and lobotomize yourself to the video games of Atificial Intelliengence piloting drones assassins while stuffing UberEat. Choose to rot in the hospice not funded by google tax optimization and end up pissing himself in misery realizing that we shame the fucked children in the head we laid for them to take over this tyranny. Choose your future in a google workshop, choose cloud digitalized life. Why would I do something like that ? I chose not to choose digital life, I chose something else. The reasons ? There's no reason. You don't need reasons when you have freedoms. 