---
layout: post
title: "Lettre à Arzhur #2"
date: 2018-01-02 14:05:14 +0100
location: TwinPeaks, France
---

Arzhur, 

Voilà maintenant près de 3 mois que je ne t'ai pas répondu. Depuis la première lettre que je t'avais rédigée [ici](https://xavcc.github.io/epistole/2017/10/10/societe_ouverte_1/) le temps semble filer entre nos doigts trop impatients de changer les choses de ce monde. Ta [dernière lettre](http://arthurmasson.xyz/fr/societe-ouverte-lettres-a-xavier/) je l'ai pourtant lue et relue des dizaines de fois.  

Lorsque que nous avions ensemble rencontré l'auteur Alain Damasio à Rennes cet automne, j'avais pris quelques notes afin de d'approfondir nos réflexions : "[Être ou ne pas être... Artificiel à Rennes avec Alain Damasio](https://xavcc.github.io/hsociety/2017/11/23/damasio-rennes.html)". Son attrait et ses conseils concernant l'expérimentation d'une société ouverte m'a fait chaud au coeur et plaisir pour toi qui lança cette initiative. 

> "La technique est une composante essentielle de la vie humaine, mais elle ne remplace pas la démocratie"

Depuis j'ai poursuivi les lectures (Bourdieu, Jacquard, Chomsky...), continué les prototypes libres et open source, réaliisé de nouveaux tests avec des technologies blockchain et rempli pas mal de documentations dans des wikis. J'ai aussi beaucoup marché ici et là. J'aime cette compostion organique de l'intelligence de la main, de la sagesse des deux pieds, de la critique du cerveau et de la profondeur du coeur.

Dans ces prérégrinations, j'ai échangé avec Inso concernant une monnaie libre, basée sur Duniter et appelée Ğ1 (Prononcée « June »), qui me semble adapatée à une société ouverte. J'utilise cette monnaie et sa technologie associée. 

> "la monnaie libre ne parle pas des mêmes libertés que le logiciel libre. Mais elle utilise une approche de la liberté qui est basée sur les mêmes principes philosophique :

> **symétrie des libertés** : la garantie que personne n'est privilégié par la définition d'une liberté donnée sur un objet particulier (le logiciel, les ressources, la monnaie)

> **non nuisance des libertés** : la liberté ce n'est pas "pouvoir faire n'importe quoi"

>**Inso**

Les libertés fondamentales, ces choses qu'aujourd'hui une économie vorace de l'attention enveloppe d'une couche de sucre pour mieux nous les faire fondre. Plus personne n'a de temps d'esprit critique disponible, plus rien à cacher...
C'est plus selfie des enfants posté par les parents eux-mêmes sur facebook que le droit inaliénable, c'est buzzword et marketing sur twitter plutôt les droits de l'Homme, c'est don gracieux des vies pirvées aux GAFAM plutôt que les libertés publiques, c'est course à la punchline type TedSpeaker plutôt que les nouveaux droits comme les garanties procédurales (cf. [CEDH](https://fr.wikipedia.org/wiki/Convention_europ%C3%A9enne_des_droits_de_l%27homme)).

Mon ami, il y a urgence de s'essayer à une société ouverte comme tu l'avais proposé, mais c'est un changement cultuel et culturel dont nous avons besoin si nous voulons que les méthodes et les outils favorisant les libertés soient utilisés par d'autres que initié.e.s actuel.le.s.

Ces dernière semaines j'ai parcouru les lignes des "[Nouveaux Léviathans](https://framablog.org/2018/01/04/les-nouveaux-leviathans-iv-la-surveillance-qui-vient/).

J'ai ainsi ressenti un trouble, ou peut être un risque, et je voudrais t'en faire part dans cette lettre. Je crois qu'une grande partie des personnes qui composent cette société non-ouverte dans son état actuelle sont sous l'effet d'un analgésique, d'une codéine de l'esprit critique injectée via des solutions technologiques par l'acte volontaire d'individus que ne cherchent qu'à conforter leur position de pouvoir en centralisant toujours plus, l'opacité étant l'une de leurs méthodes. Un empoisonnement, qui produit un état général de somnolence, largement soutenu par des personnes se revendiquant de "bienveillance" qui appuient et relaient un brouhaha informationnel et des promesses éhontées de toujours mieux ; une propagande orchestrée dont les petites mains y prennent part certainement dans l'espoir d'améliorer leur condition sociale, de gravir l'échelle de le reconnaissance par la "célébrité". La codéine est gratuite, ou plus exactement nous la payons au prix fort de nos libertés sacrifiées, de nos resources privatisées.

<div class="post-image">
    <img src="https://farm5.staticflickr.com/4152/5019518099_4851446276_z.jpg" alt="A full-size image example" />
    <p class="post-image-caption">Image By IMal | Licence CC BY SA NC</p>
</div>
 
## Sans téchnophobie ni complotisme

Comme tu le sais, j'ai eu le bonheur d'avoir un neveu pour la première fois en septembre dernier. Cela change grandement la vie de ma soeur et de son compagnon et cela influe sur ma manière de considérer l'avenir au quotidien. Depuis 16 ans maintenant j'ai choisis de ne pas andonner ce monde, dans lequel vivent les personnes que j'aime, aux silences des pantoufles. L'agilité, les libertés, l'écologie et la biosphère, s'éffondrent dans un silence complaisant, ou parfois dans des "oui mais tu comprends c'est difficile et compliqué", étouffés par un bruit ambiant de sur-abondance de communication de masse et de marketing des foules.

J'aime ce monde et j'aime les personnes qui font ce monde.

> Par vous je deviens vaste, avec vous je me peuple. Happy GNU year les ami.e.s (_J'avais lancé sur différents réseaux sociaux en cette fin d'année 2017_)

Ce monde repose sur le fragile cycle de la vie, un programme vieux de presque 4 milliards d'années avec ces lois de la physique, ces bases fondamentales de chimie, des principe de bilogie... Pendant longtemps, pendant presque toute son existance, ce cylce jouissait de libertés :

0. la liberté d'exécuter le programme, pour tous les usages ;

1. la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins ;

2. la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de faire commerce des copies) ;

3. la liberté d'améliorer le programme et de distribuer ces améliorations aux individus, pour en faire profiter toute la communauté.

Tu pourrais m'écrire qu'ici je carricature. Certes, j'ai peut être des influences Stallmanienne. D'un coté, des communicant.e.s de la codéine m'ont affublé de "Troskiste numérique" pour ces choses, alors que d'un autre coté, des membres de ma famille me considèrent comme "Macronien d'une start-up Nation". Le brouhahah empêche beaucoup de réflexions mais pas celle "de la panacée de la misère. Que de rêves pourtant se sont brisés contre ce miroir aux alouettes !" — (Jean-Marie Mollo Olinga).

Soit rassuré, je vis bien cette pseudo-schyzophrénie imposée par la foule. J'en ris parfois, mème.

Ce cylcle du vivant est soumis aux influences environnementales, comme le sont la météo ou les comportement sociaux des individus (qui eux aussi sont influencés par le cylce lui-même). Donc lorsque des entités individuées qui s’engagent intentionnellement, ou inconsciemment, à la conception d'un élément culturel reconnaissable, répliqué et transmis par l'imitation du comportement d'un individu par d'autres individus dans un processus de massification, elles influencent le cycle de manière forte, parfois tellement fortemment que cette influence prend le dessus sur le cours "naturel" de ce cycle.

Autrement écrit, ces personnes qui participent à la diffusion de la codéine pensent être en maitrise de quelque chose qu'elle ne savent pas même nommer, ni définir ou ni utiliser. Elles ne font que renforcer les choix de contrôle d'une poignée d'individus masqués dérrière le brouhaha. La codéine soulage les maux de tête. 

> Quand les bénéficiaires ne contrôlent pas un programme, c'est le programme qui contrôle les utilisateurs. Le développeur contrôle le programme, et par ce biais, contrôle les bénéficiaires. Ce programme non libre, ou « privateur », devient donc l'instrument d'un pouvoir injuste.

Comment fait-on société ouverte dans une telle absence de libertés ? As-tu des réponses à cela mon ami ? Même mon père, avec qui j'échange très peu et que je n'avais pas vu depuis des mois, m'a dit "Je m'en fous de tes histoires de liberté et d'ouverture, je n'ai rien cacher". Je n'ai pas voulu approfondir avec lui les principes qu'il semble amalgamer de vie privée et de programme ouvert.

GAFAM, NATU, instituts politiques, individus de l'arrêt de bus et personne de champ de blé, tous et toutes surfent sur une économie de l'attention, tout le monde deale de la codéine. L'orgueil pour tous et seuls très peu s'en enrrichissent. Profiter en gérant un commun comme un bien privé, profitez de cette insoutenable possibilité. Arzhur, tout le monde est coupable personne n'est plus responsable ?

Je suis en tout cas heureux de pouvoir t'écrire librement, c'est émancipateur de ces formes de colonisation, c'est par cet acte de créativité que je tente d'interroger les contours d'une société ouverte et des risques possibles qu'embarqe un tel projet dans sa mise en oeuvre et dans son imaginaire.

## Dont-on chercher le consentement de nos contemporains ?

Il n'y a de créativité possible que dans un système de règles, d'après Foucault. 
Peut-on alors en manipulant ces règles, comme le font des facilitateurs / médiateurs (médias) dans un processus d'intelligence collective, fabriquer le consentement ? Dans une organisation démocratique ? Dans une entreprise "libérée" ? Dans une holacratie ? 
Une phrase, qu'elle soit écrite, prononcée ou entendue, est contrainte à un ordre linéaire. Ses éléments peuvent être ordonnés sur un axe unique, celui du temps. Le sens de cette phrase, lui, est fondamentalement hiérarchique.

La structure syntaxique est la structure de médiation entre le sens, de structure hiérarchisée, et la réalisation phonologique des phrases, linéarisée.

Par le pouvoir de programmer, d'émettre, de diffuser, de réguler et de réceptionner, le langage, des noeuds (qui peuvent être un individu ou un groupe en position de média et en posture construction) se trouvent en capacité de structurer et de décider une fabrication d'un consentement des masses par le langage en articulant des liens ténus entre intellectualité et structure syntaxique.

La structure syntaxique est construite à partir des éléments du lexique et d'éléments fonctionnels, soudés les uns aux autres. Les relations de vérification de traits, d'accord et de cas, les mouvements liés à la structure informationnelle sont opérés dans la structure syntaxique.

Ainsi, le consentement serait la simple validation d'une expression de pouvoir d'un petit groupe d'individus sur un groupe plus étendu et assujetti à la fabrication par une élite ? (Voir : [Noam Chomsky La Fabrication du Consentement partie 1](https://youtu.be/waUIPMXuHV0)).

Arzhur, une société ouverte aurait le consentement comme véhicule de décision dans ces instances de gouvernances ?

## On se voit bientôt ?

Dans ces réflexions et ces actions, le temps file toujours de façon aussi liquide. J'espère te revoir et partager des moments de discussions et de nouveaux tests, avec toi et avec d'autres personnes, en Bretagne ? En Allemagne ? À Lyon ?

Nous devons aussi répondre au message d'Alain Damasio. J'ai également quelques lettres à écrire à [Alserweiss](https://www.wattpad.com/story/81056393-les-dragons-stochastiques) et ses dragons, et MaxLath (qui fait un beau truc avec WikiData).

À te lire et revoir très vite mon ami !

_Ksawery_
