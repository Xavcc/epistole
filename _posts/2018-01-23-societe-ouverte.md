---
layout: post
title: "Lettre à Alserweiss"
date: 2018-01-23 14:05:14 +0100
location: TwinPeaks, France
---

Alserweiss, 

Je t'écris après quelques promenades dans les vents de la terre du Kreiz Breizh. Je voulais te faire part de nos réflexions et expérimentations sur une société ouverte. Une société basée sur les principes du libre et de l'open source dont l'initiative fut lancée par Arzhur dans sa [lettre de l'année dernière](http://arthurmasson.xyz/societe-ouverte-lettres-a-xavier).

Je me suis réveillé aujourd'hui avec un sentiment étrange que je vais tenté de te décrire. Une société ouvrte est-elle possible, souhaitable et porte t-elle des risques ?

Je lisais ceci ce matin : 
"Where lives matter, such as in mission critical environments, Open Source is a risk.", dans [Effective Use of Geospatial Big Data](https://www.gis-professional.com/content/article/effective-use-of-geospatial-big-data)

<div class="post-image">
    <img src="https://farm9.staticflickr.com/8408/8763840793_28ace0dc6d_b.jpg" alt="A full-size image example" />
    <p class="post-image-caption">Our House | Image By Sabbian Paine | Licence CC BY-SA</p>
</div>

2018, dans un pays renommé #LeanFrance,
l'état et ces instances dans les contrées administrées, anciennes régions au XXè s., par des algorithmes de machine learning, est aujourd'hui un pôle de compétitivité efficace et dématérialisé.
Il optimise au mieux pour la vie et la richesse de ses citoyens, les services de la défense et de l'éducation ont été confiés à la société privée américaine Cro$oft lors d'open bar postitathon, la formation à la citoyenneté et à l'acte politique est maintenue par Alpha00gle, qui même dans la ville rebelle de Roazhon, qui avait juré de n’utiliser que du logiciel libre dans la fonction publique, dirige aujourd'hui les conduites du changement digital du mouvement indépendantiste Breizhien.
Les échanges de bien et de services, les fournitures santé, scolaire, sont permis, autorisés, régulés, à très grande vitesse et très petit prix par L'Amazone de la Valley, une puissante organisation qui répand le bonheur sur le monde et dans la #LeanFrance.
Créer les conditions pour fluidifier les conditions du changement est la baseline de l'État dématérialisé. Pour les libertés fondamentales, il est devenu marginal, et même hors la loi dans certaines contrées, tout individu qui s'engage à la liberté d'exécuter le programme, pour tous les usages ;
la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins ;
la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies) ;
la liberté d'améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté.
L'accès au code source est une condition d'exercice des libertés 1 et 3. Seuls quelques rebuts de la start-up Nation osaient encore arpenter ces sentiers.

Toutes ces actions de disruption des paradigmes, pour les changer vers la fluidité agile, avaient enclenchées après la déclaration du début de l'an 2018 :
"3,7 milliards de personnes, soit 50% de la population mondiale, n'ont pas touché le moindre bénéfice de la croissance mondiale l'an dernier. 82% de la richesse créée est allée à 1% de l'humanité. C'est un symptôme de l'échec du système économique", Winnie Byanyima".
Faisant ainsi réagir les dirigeants français du CAC40, le gouvernement récemment élu par une majorité de #LeanFrançais devenus tous actor'studio de l'écologie pour sauver l'humanité, la paix et les digitals.

Comme au XIIIè siècle, de grandes villes marchandes tenaient tête à l'État Digital Nation (EDN est un marque déposée par le mouvement pour  la république en Marche), mais elles usent aujourd'hui des mêmes Omnivore As a Service (OAS) 4.0 que l'État. Les études, sciences et techniques étaient confiées à des services d'analyse fine de Marketage et aux Instituts de Management Blockchainisés pour sécuriser et distribuer les échanges de façon juste et citoyenne.

Le paradis de l'équité, des libertés, de la démocratie participative transpirait dans le quotidien d'un peuple innovant et dans leurs licornes qu'ils attendaient comme les vikings attendaient Odin.

On pouvait lire sur les écrans des gares SNCP (Société Nationale des Chemins fer Privés) : "La guerre, c’est la paix. La liberté, c’est l’esclavage. L’ignorance, c’est la force."

C'était magnifique ! Et toutes les personnes venues pour le grand remplacement, celles venues piller nos emplois de cerveau et de temps disponible chez Cro$oft, Amazone de Valley et Aplha00gle, étaient gazées, torturées, dans dans centre de distraction pour exilés climatiques installés dans les zones pauvres de frontières de la montée des eaux. Un ordre maintenu par des unités ultra spécialisées en psychologie d'accueil par méthodes IA (Intention Artificielle), ces unités étant financés à 30% par du crowdfunding et 70% par les dons des expatriés fiscaux #LeanFrançais qui ne voulaient pas voir leur belle, riche et historique nation de souche disparaître.

La théorie économique du ruissellement trouvait là sa plus belle POC (Proof Of Concept).

Alserweiss, que penses-tu de cette société moderne ? Où peut-elle mener ? Peut-elle supporter une ouverture basée sur des principes des liberéts et de codes ouverts ? As-tu oberservé de cas concret et précis dans ton quotidien ?

En attendant de te lire ou de te croiser, je te souhaite de respirer à plein poumon les atomes d'oxygènes pas encore dédiés au minage de crypto-monnaie.
